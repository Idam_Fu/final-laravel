@extends('layout.master')

@section('content')

<div class="container mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Profile Edit</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('profile.update', ['profile' => $profile->id]) }}" method="POST" enctype="multipart/form-data">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="fullname">Full Name</label>
                    <input type="text" class="form-control" id="fullname" value="{{ old('fullname', $profile->fullname) }}" name="fullname" placeholder="Enter Full Name">
                  </div>
                  @error('fullname')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" id="address" value="{{ old('address', $profile->address) }}" name="address" placeholder="Enter Adress">
                  </div>
                  @error('address')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label for="phone">Phone Number</label>
                    <input type="text" class="form-control" id="phone" value="{{ old('phone', $profile->phone) }}" name="phone" placeholder="Enter Phone Number">
                  </div>
                  @error('phone')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label for="photo">Photo</label>
                    <input type="file" name="photo" id="photo">
                    
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
</div>

@endsection