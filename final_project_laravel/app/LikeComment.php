<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeComment extends Model
{
    protected $table = 'comment_likes';
    protected $guarded = [];
    public function user(){
        return $this->hasOne('App\User');
    }
}
