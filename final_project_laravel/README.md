1. Informasi kelompok (nomor kelompok, anggota, tema project yang diangkat) <br>
Kelompok 13 <br>
Mohamad Idam Fuadi (mohamadidam123@gmail.com) <br>
Abid Ulinnuha (ghozalia288@gmail.com) <br>
Salsabilah Khansa (salsabilahkhansa99@gmail.com) <br>
Tema : Media Sosial dengan judul Sosial Now <br>
2. Link video demo yang berisi penjelasan singkat mengenai project yang dikerjakan (mencakup ERD, template yang dipakai, code, demo web) <br>
video : https://drive.google.com/drive/folders/1z4bW161y2mH5XqkYpARGSmgJNlUXQxZh?usp=sharing <br>
ERD : public/img/erd.png <br>
Template : AdminLTE3 <br>
3. Link deploy di hosting sendiri atau heroku (opsional). <br>
Belum ada hosting